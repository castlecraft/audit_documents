# Copyright (c) 2021, Audit Documents and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import uuid

class PrevaSalesInvoice(Document):
	def autoname(self):
		self.name = str(uuid.uuid4())

	def validate(self):
		for i in self.get("item"):
			i.total = i.rate * i.quantity