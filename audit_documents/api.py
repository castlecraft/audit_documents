import frappe
from frappe import _
import requests
import json

@frappe.whitelist(allow_guest=True)
def get_docs():
	args = get_form_params()
	docs = frappe.get_list(**args)

	frappe.local.form_dict.pop("limit_page_length", None)

	return {
		"docs": docs,
		"length": get_filtered_count(),
	}


@frappe.whitelist(allow_guest=True)
def sync_sales_inv(invoice):
    inv = frappe.get_doc("Preva Sales Invoice",invoice)
    company = frappe.get_doc("Company",inv.company)
    api_key_sec = f"token {company.api_key}:{company.api_secret}"
    headers = {"Authorization": api_key_sec}
    sales_inv_url = company.url + "Sales Invoice/" + inv.erpnext_name
    customer_inv_url = company.url + "Customer"
    item_url = company.url + "Item"

    if inv.docstatus == 1:
        get_customer = requests.get(customer_inv_url,headers=headers)
        customer_data = json.loads(get_customer.text)
        
        for i in customer_data["data"]:
            if i["name"] == inv.customer:
                break
        else:
            new_cus = {"customer_name":inv.customer}
            new_customer = requests.post(customer_inv_url,data=json.dumps(new_cus),headers=headers)
        
        get_item = requests.get(item_url,headers=headers)
        item_data = json.loads(get_item.text)
        
        items = []
        for i in inv.item:
            for j in item_data["data"]:
                if j["name"] == i.item_name:
                    break
            else:
                new_itm = {
                    "item_code":i.item_name,
                    "item_group":company.item_group
                }
                new_item = requests.post(item_url,data=json.dumps(new_itm),headers=headers)

            itm = {}
            itm["item_code"] = i.item_name
            itm["income_account"] = company.income_account
            itm["qty"] = i.quantity
            itm["rate"] = i.rate
            items.append(itm)

        doc = {
            # "customer": inv.customer,
            # "items": items,
            "docstatus":1
        }
        
        new_sales_inv = requests.put(sales_inv_url,data=json.dumps(doc),headers=headers)
        if new_sales_inv.status_code == 200:
            frappe.db.set_value('Preva Sales Invoice', inv.name, 'is_synced', True)
            frappe.db.commit()
        return new_sales_inv.status_code
        
    else:
        frappe.throw("Invoice is Not Submitted")

@frappe.whitelist(allow_guest=True)
def fetch_inv(data=None):
    response = json.loads(frappe.request.data)
    company_exist = frappe.get_all("Company")
    for i in company_exist:
        if i["name"] == response["company"]:
            break
        else:
            new_comp = frappe.new_doc("Company")
            new_comp.company_name = response["company"]
            new_comp.save()
            frappe.db.commit()
    
    doc = frappe.new_doc("Preva Sales Invoice")
    for i in response["items"]:
        items = doc.append('item', {})
        items.item_name = i["item_name"]
        items.rate = i["rate"]
        items.quantity = i["qty"]
    doc.customer = response["customer"]
    doc.company  = response["company"]
    doc.posting_date = response["posting_date"]
    doc.remarks = response["remarks"]
    doc.erpnext_name = response["name"]
    doc.insert()
    frappe.db.commit()