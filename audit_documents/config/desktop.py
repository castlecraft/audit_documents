from frappe import _

def get_data():
	return [
		{
			"module_name": "Audit Documents",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Audit Documents")
		}
	]
